# 0's As A Service
So, this is a joke. It runs on Lambda, and is inspired by the Lofty Labs team
in Fayetteville, AR. You should check out their podcast at 
[friday.hirelofty.com](https://friday.hirelofty.com).

I don't have any intent on making this legit, but feel free to submit your
issues or PR's and I'll get to them. Enjoy!

Thanks to Sebastian Tiangolo for FastAPI, the Uvicorn project, and to erm on
Github for Mangum. Pretty fun little app to write!