from mangum import Mangum

"""
------------------------------
The following linter supression is an implementation detail that allows this
app pattern to work in lambda. A fix would need to be submitted in the build
process for mangum CLI, or this could be refactored.
------------------------------
"""
# pylint: disable=import-error
from app_core.main import app

handler = Mangum(app)
