from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI(title="0aas", version="0.1.1")


class ZeroResponse(BaseModel):
    zeros: str


class OneResponse(BaseModel):
    ones: str


@app.get("/")
async def root():
    return {"greeting": "Hello, World!"}


@app.post("/0/", response_model=ZeroResponse)
async def zero_post(num_zeros: int):
    return ZeroResponse(zeros=num_zeros * "0")


@app.post("/1/", response_model=OneResponse)
async def one_post(num_ones: int):
    return OneResponse(ones=num_ones * "1")
