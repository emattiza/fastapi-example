from fastapi.testclient import TestClient

from app.app_core.main import app

client = TestClient(app)


def test_get_input():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"greeting": "Hello, World!"}
