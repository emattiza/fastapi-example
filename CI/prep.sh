#!/bin/sh
#===============================================================================
#
#          FILE: prep.sh
#
#         USAGE: ./prep.sh
#
#   DESCRIPTION: ---
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Evan Mattiza (),
#  ORGANIZATION: ---
#       CREATED: 03/21/2020 12:20:28 PM
#      REVISION: ---
#===============================================================================

set -o nounset                                  # Treat unset variables as an error

pip install --user poetry
export PATH=$PATH:/root/.local/bin
poetry config virtualenvs.create true --local
poetry config virtualenvs.in-project true --local
poetry install
poetry export -o requirements.txt